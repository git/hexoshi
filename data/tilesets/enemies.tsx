<?xml version="1.0" encoding="UTF-8"?>
<tileset name="enemies" tilewidth="62" tileheight="48" tilecount="7">
 <tile id="0">
  <properties>
   <property name="cls" value="bat"/>
  </properties>
  <image width="16" height="16" source="../images/objects/enemies/bat-0.png"/>
 </tile>
 <tile id="1">
  <properties>
   <property name="cls" value="frog"/>
  </properties>
  <image width="16" height="16" source="../images/objects/enemies/frog_stand.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="cls" value="worm"/>
  </properties>
  <image width="32" height="40" source="../images/objects/enemies/worm-4.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="cls" value="mantanoid"/>
  </properties>
  <image width="32" height="48" source="../images/objects/enemies/mantanoid.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="cls" value="scorpion"/>
  </properties>
  <image width="62" height="36" source="../images/objects/enemies/scorpion.png"/>
 </tile>
 <tile id="5">
  <properties>
   <property name="cls" value="hedgehog"/>
  </properties>
  <image width="20" height="20" source="../images/objects/enemies/hedgehog.png"/>
 </tile>
 <tile id="6">
  <properties>
   <property name="cls" value="jellyfish"/>
  </properties>
  <image width="32" height="32" source="../images/objects/enemies/jellyfish.png"/>
 </tile>
</tileset>
